﻿using ListViewDemoApp.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ListViewDemoApp.Model
{
    public static class PlayerVault
    {
        public static IList<Player> Players { get; set; }

        static PlayerVault()
        {
            Players = new ObservableCollection<Player> {
                new Player
                {
                    Name = "Kobe Bryant",
                    Position = "Shooting guard",
                    Team = "Los Angeles Lakers",
                    Image = "kobe.jpg", 
                    Image1 = "Kobejersey.png" // change img
                },
                new Player
                {
                    Name = "LeBron James",
                    Position = "Power forward",
                    Team = "Cleveland Cavaliers",
                    Image = "LebronJames.jfif", // change img
                    Image1 = "LebronJersey.jfif"
                },
                new Player{
                    Name ="Michael Jordan",
                    Position = "Shooting guard",
                    Team ="Chicago Bulls",
                    Image1 = "Jordan.png", // change img
                    Image = "Jordanjersey.png"
                },
                new Player
                {
                    Name ="Magic Johnson",
                    Position = "Point guard",
                    Team = "Los Angeles Lakers",
                    Image1 = "magic.png",
                    Image = "Magicjersey.png" // change img
                },
                new Player
                {
                    Name = "Drazen Petrovic",
                    Image1 = "Petrovic.jfif",// change img
                    Image = "PetrovicJersey.png",
                    Position = "Shooting guard",
                    Team = "New Yersey nets"
                },

            };
        }
    }
}