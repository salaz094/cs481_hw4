﻿
using ListViewDemoApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_Hw4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Player> Players
        {
            get;
            set;
        }
        public MainPage()
        {
            InitializeComponent();
            CreateList();
        }
            private void CreateList()
          { 
            
            Players = new ObservableCollection<Player>();

            new Player()
            {
                Name = "Kobe Bryant",
                Position = "Shooting guard",
                Team = "Los Angeles Lakers",
                Image = "kobe.jpg",
                Image1 = "Kobejersey.png"
            };

            new Player()
            {
                Name = "LeBron James",
                Position = "Power forward",
                Team = "Cleveland Cavaliers",
                Image = "Lebum.png", // change img
                Image1 = "Lebronjers.png"
            };

            new Player()
            {
                Name = "Michael Jordan",
                Position = "Shooting guard",
                Team = "Chicago Bulls",
                Image1 = "Jordan.png", // change img
                Image = "Jordanjersey.png"
            };

            new Player()
            {
                Name = "Magic Johnson",
                Position = "Point guard",
                Team = "Los Angeles Lakers",
                Image1 = "magic.png",
                Image = "Magicjersey.png"
            };

            new Player()
            {
                Name = "Drazen Petrovic",
                Image1 = "Drezan.png",// change img
                Image = "PetrovicJersey.png",
                Position = "Shooting guard",
                Team = "New Yersey nets"
            };

            NBAGreats.ItemsSource = Players;
        }
       
        private void OnDelete(object sender, System.EventArgs e)
        {
            var listItem = ((MenuItem)sender);
            var player = (Player)listItem.CommandParameter;

            Players.Remove(player);
        }

        private void Handle_Refreshing(object sender, EventArgs e)
        {
            CreateList();// impl
            NBAGreats.IsPullToRefreshEnabled = false; //refreshData
        }

        

       
    }
}
